

import autogluon.core as ag
from autogluon.vision import ImagePredictor

#dataset_train = ImagePredictor.Dataset.from_folder('/home/alex_at_imago/data_images/liver_color/auto_fov/Sym3/train')
dataset_test = ImagePredictor.Dataset.from_folder('/home/alex_at_imago/data_images/liver_color/auto_fov/Sym3/test_ben_mal')

new_predictor = ImagePredictor.load('model_liver_ben_mal_for_anna.pkl')
test_map_from_saved_model = new_predictor.evaluate(dataset_test)
#print(f"The mAP on the test dataset is {test_map[1][-1]}")

print("Printing the test results from the saved model: ")
print(test_map_from_saved_model)
#///////////////////////////////////////////////////

print('\n')
print("Printing the test results from the saved model: ")
print(test_map_from_saved_model)

#///////////////////////////////////
print('\n')
print("Printing the test results for one image by using the saved model: ")

image_path = dataset_test.iloc[0]['image']
result_for_one_image = new_predictor.predict(image_path)
 #result_for_one_image = new_predictor.evaluate(image_path) -- wr for one image

print(result_for_one_image)

#probabilities for one image
print('\n')
print("The probabilities of the 2 classes for one image: ")
probab_for_one_image = new_predictor.predict_proba(image_path)
print(probab_for_one_image)

print('\n')
print("The results of for the test images: ")
results_all_test_images = new_predictor.predict(dataset_test)
print(results_all_test_images)

print('\n')
print("The probabilities of the 2 classes for the test images: ")
probabs_all_test_images = new_predictor.predict_proba(dataset_test)
print(probabs_all_test_images)

#///////////////////////////////////////////////////
input("The end: please press Enter to exit")
